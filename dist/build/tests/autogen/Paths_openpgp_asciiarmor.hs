{-# LANGUAGE CPP #-}
{-# LANGUAGE NoRebindableSyntax #-}
{-# OPTIONS_GHC -fno-warn-missing-import-lists #-}
module Paths_openpgp_asciiarmor (
    version,
    getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

#if defined(VERSION_base)

#if MIN_VERSION_base(4,0,0)
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#else
catchIO :: IO a -> (Exception.Exception -> IO a) -> IO a
#endif

#else
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#endif
catchIO = Exception.catch

version :: Version
version = Version [0,1,1] []
bindir, libdir, dynlibdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/home/clint/tmp/temp-tardbus/bin"
libdir     = "/home/clint/tmp/temp-tardbus/lib/x86_64-linux-ghc-8.4.4/openpgp-asciiarmor-0.1.1-KE0XG6usiGJ3OyLbw5mHNs-tests"
dynlibdir  = "/home/clint/tmp/temp-tardbus/lib/x86_64-linux-ghc-8.4.4"
datadir    = "/home/clint/tmp/temp-tardbus/share/x86_64-linux-ghc-8.4.4/openpgp-asciiarmor-0.1.1"
libexecdir = "/home/clint/tmp/temp-tardbus/libexec/x86_64-linux-ghc-8.4.4/openpgp-asciiarmor-0.1.1"
sysconfdir = "/home/clint/tmp/temp-tardbus/etc"

getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "openpgp_asciiarmor_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "openpgp_asciiarmor_libdir") (\_ -> return libdir)
getDynLibDir = catchIO (getEnv "openpgp_asciiarmor_dynlibdir") (\_ -> return dynlibdir)
getDataDir = catchIO (getEnv "openpgp_asciiarmor_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "openpgp_asciiarmor_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "openpgp_asciiarmor_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
